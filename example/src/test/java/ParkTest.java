import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParkTest {



    @Test
    void park() {
        Park car = new Park();
        car.park("1A",7);
        boolean car1 = car.park("1A",9);
        assertTrue(false == car1);
    }

    @Test
    void parkTestSize(){
        Park car = new Park();
        car.park("1A",7);
        car.park("2A",7);
        car.park("3A",7);
        car.park("4A",7);
        car.park("5A",7);
        car.park("6A",7);
        car.park("7A",7);
        car.park("8A",7);
        car.park("9A",7);
        car.park("10A",7);
        car.park("11A",7);
        boolean car1 = car.park("12A",7);
        assertTrue(false == car1);
    }

    @Test
    void unpark() { // обычная дневная парковка с 7 утра до 22 вечера.
        Park car = new Park();
        car.park("1A",7);
        int car1 = car.unpark("1A",21);

        assertEquals(700,car1);
    }

    @Test
    void unparkFewerSeven(){ // время парковки меньше 7 часов и время ухода тоже меньше семи (не учитывается время больше одного дня).
        Park car = new Park();
        car.park("1A",1);
        car.park("2A",2);
        car.park("3A",3);
        car.park("4A",4);
        car.park("5A",5);
        car.park("6A",6);
        car.park("7A",7);

        int a = car.unpark("1A",1);
        int a1 = car.unpark("2A",3);
        int a2 = car.unpark("3A",4);
        int a3 = car.unpark("4A",5);
        int a4 = car.unpark("5A",6);
        int a5 = car.unpark("6A",7);
        int a6 = car.unpark("7A",7);

        assertEquals(0,a);
        assertEquals(30,a1);
        assertEquals(30,a2);
        assertEquals(30,a3);
        assertEquals(30,a4);
        assertEquals(30,a5);
        assertEquals(0,a6);
    }

    @Test
    void unparkFewerSevenPart2(){ // время парковки меньше 7 часов, время ухода с 7 утра до 22 вечера.
        Park car = new Park();
        car.park("1A",1);
        car.park("2A",2);
        car.park("3A",3);
        car.park("4A",4);

        int a = car.unpark("1A",7);
        int a1 = car.unpark("2A",8);
        int a2 = car.unpark("3A",21);
        int a3 = car.unpark("4A",22);

        assertEquals(180,a);
        assertEquals(200,a1);
        assertEquals(820,a2);
        assertEquals(840,a3);
    }

    @Test
    void unparkFewerSevenPart3(){ // время парковки меньше 7 часов, время ухода с 7 утра до 24 вечера.
        Park car = new Park();
        car.park("1A",1);
        car.park("2A",2);
        car.park("3A",3);
        car.park("4A",4);
        car.park("5A",4);

        int a = car.unpark("1A",7);
        int a1 = car.unpark("2A",8);
        int a2 = car.unpark("3A",22);
        int a3 = car.unpark("4A",23);
        int a4 = car.unpark("5A",24);

        assertEquals(180,a);
        assertEquals(200,a1);
        assertEquals(870,a2);
        assertEquals(870,a3);
        assertEquals(900,a4);
    }

    @Test
    void unparkMoreSeven(){ //время парковки с 22 до 24 часов, время ухода с 7 утра до 22 вечера.
        Park car = new Park();
        car.park("1A",22);
        car.park("2A",23);
        car.park("3A",24);

        int a = car.unpark("1A",7);
        int a1 = car.unpark("2A",21);
        int a2 = car.unpark("3A",22);

        assertEquals(270,a);
        assertEquals(940,a1);
        assertEquals(960,a2);
    }

    @Test
    void unparkMoreSevenPart1(){ //время парковки с 22 до 24 часов, время ухода меньше 7 утра
        Park car = new Park();
        car.park("1A",22);
        car.park("2A",23);
        car.park("3A",24);
        car.park("4A",24);

        int a = car.unpark("1A",7);
        int a1 = car.unpark("2A",6);
        int a2 = car.unpark("3A",5);
        int a3 = car.unpark("4A",1);

        assertEquals(270,a);
        assertEquals(210,a1);
        assertEquals(150,a2);
        assertEquals(30,a3);
    }

    @Test
    void show() {
    }


}