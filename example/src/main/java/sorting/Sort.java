package sorting;

import java.io.*;
import java.text.Collator;
import java.util.*;
import java.util.stream.Collectors;

public class Sort {
    public static void main(String[] args) {

        try {
            FileInputStream file = new FileInputStream("words.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader((file)));

            List<String> strings;
            //добавление прочитанных с файла строк в коллекцию
            strings = br.lines().collect(Collectors.toList());

            List<String> arrayWithCount = new ArrayList<>();

            // учет особенностей языка
            Collator collator = Collator.getInstance(new Locale("ru"));

            // добавляем кол-во повторений строки
            for (int i = 0; i < strings.size(); i++) {
                arrayWithCount.add(i, strings.get(i) + " " + Collections.frequency(strings, strings.get(i)));
            }

            Scanner scanner = new Scanner(System.in);
            System.out.println("Ввведите цифру от 1 до 3, где "+
                    '\n' + "1 - сортировка по алфавиту" +
                    '\n' + "2 - сортировка по кол-ву символов в строке" +
                    '\n' + "3 - по слову в строке");

            switch (scanner.nextInt()){
                case 1:
                    arrayWithCount.sort(collator);
                    break;
                case 2:
                    arrayWithCount.sort(Comparator.comparing(String::length));
                    break;
                case 3:
                    System.out.println("Введите цифру слова в строке (подсчет слов с 0)");
                    arrayWithCount = sort(scanner.nextInt(),arrayWithCount);
                    break;
                }

            arrayWithCount.forEach(System.out::println);

            FileWriter writer = new FileWriter("wordsResult.txt");
            for (String s : arrayWithCount) {
                writer.write(s);
                writer.write(System.getProperty("line.separator"));
            }

            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<String> sort(int input, List<String> arrayWithCount){
        List<List<String>> mainList = new ArrayList<>();
        List<String> list = new ArrayList<>();

        //разделяем строки из пришедшего arrayWithCount на слова и добавляем их в коллекцию mainList
        for (String s : arrayWithCount) {
            String[] x = s.split(" ");
            Collections.addAll(list, x);
            mainList.add(list);
            list = new ArrayList<>();
        }
        // переопределяем компаратор и сортируем строки на основе выбранных слов.
        class ListComparator implements Comparator<List<String>> {
            @Override
            public int compare(List<String> o1, List<String> o2) {
                return o1.get(input).compareTo(o2.get(input));
            }
        }

        mainList.sort(new ListComparator());

        //склеивание всех массивов в list
        for (List<String> s : mainList) {
            String str1 = String.join(" ",s);
            list.add(str1);
        }

        return list;
    }

}
