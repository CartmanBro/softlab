import java.util.HashMap;
import java.util.Map;

public class Park {

    private Map<String,Integer> placePark = new HashMap<String,Integer>();

    private int price = 50;
    private int discount = 30;

    public boolean park(String id, int firstHour){
        if(placePark.containsKey(id)){
            System.out.println("Место уже занято");
            return false;
        }
        else{
            if(placePark.size() <= 10){
                System.out.println("Машина запаркована на место: " + id + " в " + firstHour + ":00");
                placePark.put(id,firstHour);
                return true;
            }
            else
                System.out.println("На парковке нет мест" );
                return false;

        }
    }
    public int unpark(String id,int lastHour){
        System.out.println();
        int total = 0;
        int firstHour = placePark.get(id);
        if(firstHour < 7){
            if(lastHour > 7 && lastHour <= 22){
                showResult(id,lastHour, (((7-firstHour)*discount)+((lastHour - 7)*price)));
                total = ((7-firstHour)*discount)+((lastHour - 7)*price);
                placePark.remove(id);
            }
            else if (lastHour > 7 && lastHour <=24){
                int x = 24 - lastHour;
                if(x == 0){
                    x = 2;
                    showResult(id,lastHour,(((7-firstHour+x)*discount)+((lastHour - 7-x)*price)));
                    total = ((7-firstHour+x)*discount)+((lastHour - 7-x)*price);
                    placePark.remove(id);
                }
                else{
                    showResult(id,lastHour,(((7-firstHour+x)*discount)+((lastHour - 7-x)*price)));
                    total = ((7-firstHour+x)*discount)+((lastHour - 7-x)*price);
                    placePark.remove(id);
                }
            }
            else {
                showResult(id,lastHour,(lastHour - firstHour)*discount);
                total = (lastHour - firstHour)*discount;
                placePark.remove(id);
            }
        }
        else if(firstHour >= 22 && firstHour <=24){
            int i = 24-firstHour;
            if (lastHour >= 7 && lastHour <= 22) {
                showResult(id,lastHour,(((7 + i) * discount) + ((lastHour - 7) * price)));
                total = ((7 + i) * discount) + ((lastHour - 7) * price);
                placePark.remove(id);
            }
            else {
                showResult(id,lastHour,(lastHour+i)*discount);
                total = (lastHour+i)*discount;
                placePark.remove(id);
            }
        }

        else
        {
            System.out.println("Место: "+ id + " освободилось в " + lastHour + ":00" + '\n' + "Ваш остаток без учета ночной скидки: " +(lastHour - firstHour)*price + " р.");
            total = (lastHour - firstHour)*price;
            placePark.remove(id);
        }
        return total;
    }

    private void showResult(String id, int lastHour, int total){
        System.out.println("Место: "+ id + " освободилось в " + lastHour + ":00" + '\n' + "Ваш остаток c учетом ночной скидки: " + total + " р.");
    }

    public void show(){
        System.out.println();
        if(placePark.isEmpty()){
            System.out.println("Парковка полностью пуста");
        }
        else{
            System.out.println("Занятые места: ");
            for (Map.Entry<String, Integer> s : placePark.entrySet() ){
                System.out.println("Место: "+s.getKey()+" запаркована в: " + s.getValue() + ":00");
            }
        }
    }


}
